def is_pangram(string):
    alphabet = "abcdefghijklmnopqrstuvwxyz"
    string = string.replace('.', '').lower()
    return not (set(alphabet) - set(string))
